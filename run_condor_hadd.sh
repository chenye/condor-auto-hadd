#!/bin/bash
######################################################
######################################################
###                                                 ##
###   This is a program to submit condor job        ## 
###                                                 ##
######################################################
######################################################

#-----------------------------------
#   Dir path
#-----------------------------------
MyLocation=$(pwd)

#---------------------------------------------
# get Input file list (sample file name list)
#---------------------------------------------
InputDataTxt=$1
TargetDir=$2
FilesPerJob=$3
accounting_group=$4 # long or short 

echo "InputDataTxt :$InputDataTxt"
echo "TargetDir    :$TargetDir   "
echo "FilesPerJob  :$FilesPerJob "

#----------------------------------
#    Clean Target Dir 
#-----------------------------------
mkdir -p $TargetDir
rm -rf ${TargetDir}/OutDir*

#-----------------------------------
#     fix number ofjob 
#-----------------------------------
#j=5
#tot_files=$( cat $InputDataTxt | wc -l )
#echo "total files: $tot_files"
#rem=$(( $tot_files%$j ))
#files_per_job=$(( $tot_files/$j ))
#njob=$j
#if [ $rem -ne 0 ]; then
#   files_per_job=$(( $files_per_job+1 ))
#fi
#echo "files per job: $files_per_job"
#echo "njob: $njob"

#-----------------------------------
#     fix files per job 
#-----------------------------------
files_per_job=$FilesPerJob
tot_files=$( cat $InputDataTxt | wc -l )
njob=$(( $tot_files/$files_per_job ))
rem=$(( $tot_files%$files_per_job ))
if [ $rem -ne 0 ]; then
   njob=$(( ($tot_files/$files_per_job)+1 ))
fi

#-----------------------------------
echo "files per job: $files_per_job"
echo "total files: $tot_files"
echo "files per job: $files_per_job"
echo "njob: $njob"

#-----------------------------------
#     submit condor scripts  
#-----------------------------------
for((i=0;i<$njob;i++));
do
  ## dir operation  
  mkdir ${TargetDir}/OutDir$i
  export WorkDir="${TargetDir}/OutDir$i"
  echo "WorkDir:" ${WorkDir}

  ## start & end file 
  start_file=$(( $i*$files_per_job+1 ))
  end_file=$(( $start_file+$files_per_job-1 ))
  echo "start file: $start_file   end file: $end_file"
  
  ## make inputdata.txt file 
  sed -n $start_file\,${end_file}p $InputDataTxt > tmp.txt
  mv tmp.txt ${WorkDir}/inputdata.txt
   
  pushd ${WorkDir}

  ## copy hadd.sh
  cp ${MyLocation}/hadd.sh .   #change here

  ## make ff.sub file 
  cat >>ff.sub<< EOF
Executable                    = hadd.sh 
Universe                      = vanilla
Notification                  = Never
GetEnv                        = True
Arguments                     = ${WorkDir}
Output                        = test.out
Error                         = test.err
Log                           = test.log
should_transfer_files         = yes
transfer_input_files          = ${WorkDir}/inputdata.txt
WhenToTransferOutput          = ON_EXIT_OR_EVICT
accounting_group              = ${accounting_group}

Queue
EOF

 ## sumbmit condor job 
 condor_submit ff.sub

 popd
done
