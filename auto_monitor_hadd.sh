#!/bin/sh
###############################################################################################################################
###############################################################################################################################
##                                                                                                                           ##  
##  Name     : Auto hadd monitor (ROOT)                                                                                      ##
##  Function : Make a combination of a large quantity of root file  ==> On Condor                                            ##
##  Author   : Ye Chen （USTC）                                                                                              ##
##                                                                                                                           ##
##  功能： 利用condor合并多个root文件                                                                                        ##
##  作者： 陈烨（USTC）                                                                                                      ##
##                                                                                                                           ##
##  README  :                                                                                                                ##
##    1. All you need is to change the variable in "Initialize"                                                              ##
##    2. Default ROOT Setup in file "hadd.sh"                                                                                ##
##    3. If you need to change ROOT version please change "hadd.sh "                                                         ##
##    4. root file combination level in dir: OriginFiles ==>> hadd_level0 ==>> hadd_level1 ==>> hadd_level2 ==>> ......      ##
##    5. Suggest to use "nohup sh auto_monitor_hadd.sh &" to run                                                             ##
##                                                                                                                           ##
##  说明  ：                                                                                                                 ##
##    1. 你只需要更改 “Initialize” 部分                                                                                      ##
##    2. 默认的 ROOT Setup 的环境变量在 “hadd.sh” 中                                                                         ##
##    3. 如果你需要更改ROOT 的环境请更改 "hadd.sh"                                                                           ##
##    4. root file 合并的层级 : OriginFiles ==>> hadd_level0 ==>> hadd_level1 ==>> hadd_level2 ==>> ......                   ##
##    5. 建议运行使用命令"nohup sh auto_monitor_hadd.sh &"                                                                   ##
##                                                                                                                           ##
###############################################################################################################################
###############################################################################################################################
#------------------------------------------------------------------------------
# Initialize
# 初始化变量
#------------------------------------------------------------------------------
 # your condor user name | 你的condor 用户名
USER="chenye"  
# root file you want to combine in 1 condor run  | 合并的文件数, 每个condor run
FileNumber=5   
# Output dir to save results  | 输出文件目录
OutPutDir="/lustre/AtlUser/chenye/RPC/Run_all/hadd/condor/Result" 
# input file list ｜ 你的出入文件列表
InputDataList="filelist/inputdata.txt" 
# queue for condor accouting_group variable : "short" or "long" | condor 队列 ： “short” 或者 “long”
accouting_group="short" 
# check if condor finished( time interval) | 检查condor是否运行结束的时间间隔 
check_time_interval="10m"
# 环境变量
TestArea=$(pwd)

#------------------------------------------------------------------------------
# 函数: CheckCondor 
# 功能: 检查一个condor 状况
#------------------------------------------------------------------------------
CheckCondor()
{
  echo "-------------------------------------------------------"
  echo "                CheckCondor                            "
  echo "-------------------------------------------------------"
  PROCESS_NUM=`condor_q $USER | grep "Total for query"| grep "0 jobs"| wc -l`
  condor_q $USER | grep "Total for query"
  echo "PROCESS_NUM : $PROCESS_NUM "
  echo "-------------------------------------------------------"
  return $PROCESS_NUM
}

#------------------------------------------------------------------------------
# main  
# 主体部分
#------------------------------------------------------------------------------

# timer start  
startTime=$(date "+%s")

#default setting 
Finish_hadd=0
I=0
InputFileList="filelist/inputdata_level${I}.txt"
cp $InputDataList $InputFileList

# hadd_level Loop
while (( $Finish_hadd == 0 ))
do
  #---------------------------------
  #   Initialize  
  #--------------------------------
  TarDir=$OutPutDir/hadd_level${I}
  InputFileList="filelist/inputdata_level${I}.txt"

  echo "#######################################################"
  echo "-------------------------------------------------------"
  echo  " Need hadd or not (level $I) " 
  echo "-------------------------------------------------------"

  FileNum=`cat $InputFileList | wc -l`
  if [ $FileNum -eq 1 ]
  then 
    echo "No need "
    Finish_hadd=1
    echo "#######################################################"
    break 
  else 
    echo " Need "
  fi
  
  mkdir -p $TarDir

  echo "-------------------------------------------------------"
  echo  " Run Condor level$I (level $I)" 
  echo "-------------------------------------------------------"
    CheckCondor 
    CheckCondor_RET=$?
    echo "InputFileList:  $InputFileList "
    echo "TarDir:         $TarDir " 
    echo "FileNumber:         $FileNumber " 
    source $TestArea/run_condor_hadd.sh $InputFileList $TarDir $FileNumber $accouting_group 

  #---------------------------------
  #   Condor Monitor - according to bash jobs 
  #--------------------------------
  #Condor_Finish=0 # 0 means have job running Not Finish
  #while (( Condor_Status == 0 ))
  #do
  #  CheckCondor  
  #  Condor_Finish=$?
  #  sleep 100
  #done

  echo "-------------------------------------------------------"
  echo " Condor Monitor - according to test.out (level $I)"
  echo "-------------------------------------------------------"

  FinishStatus=0
  while [ $FinishStatus -eq 0 ]
  do
    dir_num=0 # 0 means have job running Not Finish
    finish_num=0

    for dir in $TarDir/* 
    do
      echo "dir: $dir"
      finish=`cat ${dir}/test.out | grep FINISH | wc -l`
      let "dir_num+=1"
      if [ $finish -eq 1 ]
      then
        let "finish_num+=1"
      fi
    done

    echo "dir_num : $dir_num"
    echo "finish_num : $finish_num"

    if [ $finish_num -eq $dir_num ]
    then 
      echo " Finish File "
      FinishStatus=1
      break 
    fi
    sleep $check_time_interval
    echo "---------------------------------"
  done 

  echo "-------------------------------------------------------"
  echo " make list   (level $I)"
  echo "-------------------------------------------------------"

  let "I+=1"
  NewInputlist="filelist/inputdata_level${I}.txt"
  if [ -f $NewInputlist ]
  then
    rm $NewInputlist
  fi
  ls $TarDir/Out*/new_result.root >> $NewInputlist
  ls $TarDir/Out*/new_result.root 
  echo "NewInputlist : $NewInputlist"

done 
#copy result file 
cp $(cat $InputFileList) $OutPutDir/FinalResult.root

#timer : stop 
stopTime=$(date "+%s")
TimeInterval=$((stopTime-startTime))
sectime=$((TimeInterval%60))
mintime=$((TimeInterval/60))
hourtime=$((mintime/60))
mintime=$((mintime%60))
echo " Time used: $hourtime hour $mintime min $sectime sec "
