#!/bin/bash
echo "------------------setting up environment ATLAS--------------------"
echo "HOSTNAME :$(hostname)"
echo "setup ATLAS"
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_TutorialData=/lustre/AtlUser/kuhan/TutorialData/cern-june2016
export ALRB_rootVersion=6.14.04-x86_64-slc6-gcc62-opt
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup root
echo "----------------------  runing  hadd ------------------------------"
##########################
# work place
##########################
export WorkDir=$1
echo "WorkDir : $WorkDir"
pushd ${WorkDir}  # pusd 
pwd

##########################
# copy file 
##########################
file_i=0
for line in $(cat inputdata*.txt)
do
  echo "Copy File: $line" 
  file_i=$[file_i+1]
  cp $line  file_${file_i}.root
  ls
done

##########################
# hadd 
##########################
if [ -f "file_1.root" ]
then
  hadd new_result.root file_*.root
fi

##########################
# rm file_i.root 
##########################
rm file_*
popd  # popd
pwd 

##########################
# Mark as FINISH 
# this is very important
# Use to judge job finish
##########################
echo  "FINISH"
